/*
Преобразует координаты вершины из локальной системы координат в Clip Space.
Копирует цвет вершины из вершинного атрибута в выходную переменную color.
*/

#version 330

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexColor;

out vec4 color;

void main()
{
    color = vec4(vertexColor, 1.);
    gl_Position = vec4(vertexPosition, 1.);
}
