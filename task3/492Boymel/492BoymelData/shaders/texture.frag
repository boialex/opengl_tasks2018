/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
	vec3 dir; // направление взгляда (для spot)
};
uniform LightInfo light[2];

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in vec3 tangentCamSpace;

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;


void main()
{
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
	vec3 textNormal = texture(normalTex, texCoord).xyz * 2 - 1;

    vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 tng = normalize(tangentCamSpace);
	vec3 bitng = normalize(cross(normal, tng));

	normal = textNormal.x * tng + textNormal.y * bitng + textNormal.z * normal;
    normal = normalize(normal);
	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

    vec3 color = vec3(0.0);

    for (int i = 0; i < 2; ++i)
    {
        vec3 lightDirCamSpace = normalize(light[i].pos - posCamSpace.xyz); //направление на источник света

        float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

        float r2 = pow(length(light[i].pos - posCamSpace.xyz), 2);
        float atten = 1.5 / (r2 + 1.);

        if (i == 1) {
            float cos_angle = dot(light[i].dir, lightDirCamSpace.xyz);
//            atten *= pow(cos_angle, 20);
            if (cos_angle < 0) {
                atten = 0;
            }
            if (cos_angle < 0.94) {
                atten *= pow(cos_angle/2, 2);
            }
        }

        color += diffuseColor * (light[i].La + light[i].Ld * NdotL) * atten;

        if (NdotL > 0.0)
        {
            vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

            float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
            blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
            color += light[i].Ls * Ks * blinnTerm * atten;
        }
    }
	fragColor = vec4(color, 1.0);
}
