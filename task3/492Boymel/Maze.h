//
// Created by Alexander Boymel on 08.03.18.
//

#ifndef STUDENTTASKS2017_MAZE_H
#define STUDENTTASKS2017_MAZE_H

#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <sstream>
#include <random>

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Camera.hpp"
#include "common/LightInfo.hpp"
#include "common/Texture.hpp"


// walls: up, bottom, -row, +row, -column, +column

constexpr unsigned int LightNum = 2;

class MazeFreeCameraMover;

class MazeApplication: public Application, public std::enable_shared_from_this<MazeApplication> {
public:
    MazeApplication(std::string _mazeFilename, float _cellSize=1., bool _faceCamera=false);

    void makeScene() override;
    void draw() override;
    void updateGUI() override;
    void initGL() override;
    void handleKey(int key, int scancode, int action, int mods) override;
    void initFramebuffer();
    void drawToFramebuffer();

    glm::vec3 getMove(glm::vec3 newMove, glm::vec3 oldMove);

    float getCellSize();

private:
    std::vector<std::vector<int>> maze;
    float cellSize;
    int width, height;
    int cellCount;
    bool faceCamera;

    int windowWidth, windowHeight;

    std::unordered_map<char, glm::vec3> normalMap;

    std::vector<MeshPtr> cellsMesh;
    std::vector<glm::mat4> cellsTranslations;

    std::vector<MeshPtr> pictures;
    std::vector<size_t> pictureTextIndexes;

    ShaderProgramPtr shader;
    ShaderProgramPtr markerShader;
    ShaderProgramPtr _quadShader;

    //Переменные для управления положением одного источника света
    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    LightInfo _light[LightNum];

    MeshPtr marker;
    MeshPtr spot;
    GLuint _sampler;

    std::vector<size_t> textureIndexes;
    std::vector<TexturePtr> textures;
    std::vector<TexturePtr> normals;

    glm::vec3 newPortalPos;
    std::vector<std::pair<glm::vec2, glm::vec2>> portalBoxes;
    std::vector<glm::vec3> teleportPos;
    std::vector<MeshPtr> portals;
    size_t portalTextureIndex;
    size_t currentPortalIndex = 0;
    int teleported = -1;

    std::shared_ptr<MazeFreeCameraMover> faceCameraPtr;
    CameraMoverPtr orbitCameraPtr;

    int _maxAvailableAnisotropy;

    int _maxAnisotropy = 4;

    GLuint _framebufferId;
    GLuint _renderTexId;
    CameraInfo _fbCamera;
    unsigned int _fbWidth = 1024;
    unsigned int _fbHeight = 1024;

    std::vector<MeshPtr> mapCells;
    MeshPtr mapGG;
    std::vector<MeshPtr> mapPortals;

    std::vector<char> getWalls(int i, int j);
    void fillCellVertices(int i, int j, std::vector<glm::vec3> & cellVertices);
    void addCell(int i, int j, const std::vector<char> & walls);

    static std::vector<glm::vec3> getWallVertices(char type, const std::vector<glm::vec3> & cellVertices);

    MeshPtr makeCellMesh(int i, int j, char type);

    bool checkCell(int i, int j, float x, float y, int pos_i, int pos_j);

    MeshPtr makePicture(int i, int j, char type);
    MeshPtr makePortal(int i, int j, char type);
    MeshPtr makeMapCell(int i, int j);
    MeshPtr makeMapGG();
    void makeMapPortals();
};


class MazeFreeCameraMover : public CameraMover
{
public:
    MazeFreeCameraMover( MazeApplication * mazeApp);

    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    void update(GLFWwindow* window, double dt) override;

    glm::vec3 getPos();
    glm::quat getRot();

protected:
    glm::vec3 _pos;
    glm::quat _rot;

    //Положение курсора мыши на предыдущем кадре
    double _oldXPos = 0.0;
    double _oldYPos = 0.0;

    MazeApplication* maze;
};


#endif //STUDENTTASKS2017_MAZE_H
